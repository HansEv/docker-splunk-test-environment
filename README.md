The TESTenv.sh bash script is an extraction from the awesome GitHub repository   https://github.com/mhassan2/splunk-n-box with which you can easily build Splunk server instances, single Cluster instances or even Multi site Cluster instances just on your own laptop.

The TESTenv.sh just copies some of the scripted lines to create 3 Splunk test servers TEST01, TEST02 and TEST03 with a default splunk/splunk:latest image (can be changed). It then sets default passwords and makes an SMT login background available.
Remark: Only the OSX scripted lines are used, when you want to use it on other docker hosting system you probably have to check the original project for some differences.

Just place the two files (**TESTenv.sh and logo_SMT_green.png**) in some folder and run the command: ```bash ./TESTenv.sh```

The result will be three clean Splunk Test environments:
TEST01 on 10.0.0.121:8000,
TEST02 on 10.0.0.121:8000,
TEST03 on 10.0.0.121:8000

You then can set up roles and connections between them any way you wish.

#### Adding Universal Forwarders:
The UF0x.sh bash script can be used to set up 2 Universal Forwarders on the same docker network (splunk-net). Just open command prompt (is easyest with Kitematic or docker exec -it UF01 /bin/bash) and type bin/splunk add forward-server IP:9997 (the internal IP of server TEST01 can be found by: docker inspect TEST01| grep IPAddress)

#### Remark for iMac network:
Docker for Mac uses Hyperkit to run a xhyve VM for the Docker daemon. The VM uses VPNKit for exposing container ports to localhost, but the network settings and adapters are not configurable, meaning that containers cannot be accessed via their IPs and there is no network interface which bridges between the physical machine and the virtual machine.
Therefore when you reboot your laptop you need to recreate the network aliases by ```sudo ifconfig lo0 alias $CONTAINER_IP```
You can use the bash script **TESTenv_netw.sh** for that.

*Happy testing!*
