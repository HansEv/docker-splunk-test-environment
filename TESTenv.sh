#extract from the big bash file of splunknbox.sh, see: https://github.com/mhassan2/splunk-n-box

DNSSERVER=`scutil --dns|grep nameserver|awk '{print $3}'|sort -u|tail -1`
echo "DNSSERVER= " $DNSSERVER

# Set default ports
SPLUNKNET="splunk-net"				#default name for network (host-to-host comm)
#Splunk standard ports
#SSHD_PORT="8022"					#in case we need to enable sshd, not recommended
#SPLUNKWEB_PORT="8000"				#port splunkd is listing on
#SPLUNKWEB_PORT_EXT="8000"			#port mapped during docker run (customer facing)
#MGMT_PORT="8089"
#KV_PORT="8191"
#RECV_PORT="9997"
#REPL_PORT="9887"
#HEC_PORT="8088"
#APP_SERVER_PORT="8065"				#new to 6.5
#APP_KEY_VALUE_PORT="8191"			#new to 6.5

# set the required 3 server names and ip addresses
fullhostname1=TEST01                            
vip1=10.0.0.121
fullhostname2=TEST02
vip2=10.0.0.122
fullhostname3=TEST03
vip3=10.0.0.123

sudo ifconfig lo0 $vip1 255.255.255.0 alias     # reserve 3 ip adresses for subnetmask on osx on default ethernet card lo0
sudo ifconfig lo0 $vip2 255.255.255.0 alias
sudo ifconfig lo0 $vip3 255.255.255.0 alias

#-----------splunk-net check---------------
SPLUNKNET="splunk-net"				#default name for network (host-to-host comm)
printf "Checking if docker network is created [$SPLUNKNET]...\n"
net=`docker network ls | grep $SPLUNKNET `
if [ -z "$net" ]; then
	printf "$\033[0;32m Creating...network\n"
	echo -e "\033[1m\033[0m"
    docker network create -o --iptables=true -o --ip-masq -o --ip-forward=true $SPLUNKNET
else
    printf " OK\n"
fi
#-----------end of splunk-net check---------------

echo -e "\033[0;32mStarting server TEST01 on: "  $vip1
echo -e "\033[1m\033[0m"
docker run -d \
--network=$SPLUNKNET --hostname=$fullhostname1 --name=$fullhostname1 --dns=$DNSSERVER \
-e "SPLUNK_START_ARGS=--accept-license" \
-e "SPLUNK_PASSWORD=test0001" \
-p $vip1:8000:8000 -p $vip1:8089:8089 -p $vip1:8022:8022 \
-p $vip1:8088:8088 \
-p $vip1:9997:9997 -p $vip1:9887:9887 -p $vip1:8065:8065 \
-p $vip1:8191:8191 \
-e SPLUNK_ENABLE_LISTEN=9997 -e SPLUNK_SERVER_NAME=$fullhostname1 \
-e SPLUNK_SERVER_IP=$vip1 -e SPLUNK_USER=splunk  splunk/splunk:latest

echo "pausing for server to be ready"
sleep 30

docker exec -u splunk -d $fullhostname1 touch /opt/splunk/etc/.ui_login	#prevent first time changeme password screen
docker exec -u splunk -d $fullhostname1 mkdir /opt/splunk/etc/apps/search/appserver/static/logincustombg/
docker cp logo_SMT_green.png $fullhostname1:/opt/splunk/etc/apps/search/appserver/static/logincustombg/	# Set SMT default login background


echo -e "\033[0;32mStarting server TEST02 on: " $vip2
echo -e "\033[1m\033[0m"
docker run -d \
--network=$SPLUNKNET --hostname=$fullhostname2 --name=$fullhostname2 --dns=$DNSSERVER \
-e "SPLUNK_START_ARGS=--accept-license" \
-e "SPLUNK_PASSWORD=test0002" \
-p $vip2:8000:8000 -p $vip2:8089:8089 -p $vip2:8022:8022 \
-p $vip2:8088:8088 \
-p $vip2:9997:9997 -p $vip2:9887:9887 -p $vip2:8065:8065 \
-p $vip2:8191:8191 \
-e SPLUNK_ENABLE_LISTEN=9997 -e SPLUNK_SERVER_NAME=$fullhostname2 \
-e SPLUNK_SERVER_IP=$vip2 -e SPLUNK_USER=splunk  splunk/splunk:latest

echo "pausing for server to be ready"
sleep 30

docker exec -u splunk -d $fullhostname2 touch /opt/splunk/etc/.ui_login	#prevent first time changeme password screen
docker exec -u splunk -d $fullhostname2 mkdir /opt/splunk/etc/apps/search/appserver/static/logincustombg/
docker cp logo_SMT_green.png $fullhostname2:/opt/splunk/etc/apps/search/appserver/static/logincustombg/	# Set SMT default login background


echo -e "\033[0;32mStarting server TEST03 on: " $vip3
echo -e "\033[1m\033[0m"
docker run -d \
--network=$SPLUNKNET --hostname=$fullhostname3 --name=$fullhostname3 --dns=$DNSSERVER \
-e "SPLUNK_START_ARGS=--accept-license" \
-e "SPLUNK_PASSWORD=test0003" \
-p $vip3:8000:8000 -p $vip3:8089:8089 -p $vip3:8022:8022 \
-p $vip3:8088:8088 \
-p $vip3:9997:9997 -p $vip3:9887:9887 -p $vip3:8065:8065 \
-p $vip3:8191:8191 \
-e SPLUNK_ENABLE_LISTEN=9997 -e SPLUNK_SERVER_NAME=$fullhostname3 \
-e SPLUNK_SERVER_IP=$vip3 -e SPLUNK_USER=splunk  splunk/splunk:latest

echo "pausing for server to be ready"
sleep 30

docker exec -u splunk -d $fullhostname3 touch /opt/splunk/etc/.ui_login	#prevent first time changeme password screen
docker exec -u splunk -d $fullhostname3 mkdir /opt/splunk/etc/apps/search/appserver/static/logincustombg/
docker cp logo_SMT_green.png $fullhostname3:/opt/splunk/etc/apps/search/appserver/static/logincustombg/	# Set SMT default login background



#-------web.conf stuff for making new login-screen-------
PROJ_DIR="/Users/${USER}"  #anything that needs to copied to container

container_ip1=`docker inspect $fullhostname1| grep IPAddress |grep -o '[0-9]\+[.][0-9]\+[.][0-9]\+[.][0-9]\+'| head -1  `
container_ip2=`docker inspect $fullhostname2| grep IPAddress |grep -o '[0-9]\+[.][0-9]\+[.][0-9]\+[.][0-9]\+'| head -1  `
container_ip3=`docker inspect $fullhostname3| grep IPAddress |grep -o '[0-9]\+[.][0-9]\+[.][0-9]\+[.][0-9]\+'| head -1  `

echo "Changing login screen server TEST01"
LINE1="<CENTER><H1><font color=\"lime\"> SPLUNK TEST Environment   </font></H1><br/></CENTER>"
LINE2="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Hostname: </font><font color=\"#FF9033\"> $fullhostname1</font></H3>"
LINE3="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Host IP: </font><font color=\"#FF9033\"> $vip1</font></H3></CENTER>"
LINE4="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Cluster Label: </font><font color=\"#FF9033\"> geen</font></H3><BR/></CENTER>"
LINE5="<H2><CENTER><font color=\"#867979\">User: </font> <font color=\"red\">admin</font> &nbsp&nbsp<font color=\"#867979\">Password:</font> <font color=\"red\"> test0001</font></H2></font></CENTER><BR/>"
LINE6="<CENTER><font color=\"#867979\">Created using Splunk N' A Box scripts<BR/> Docker image [splunk/splunk:latest]</font></CENTER>"
custom_web_conf1="[settings]\nlogin_content=<div align=\"right\" style=\"border:1px solid lime;\"> $LINE1 $LINE2 $LINE3 $LINE4 $LINE5 $LINE6 </div> <p>This data is auto-generated at container build time (container internal IP=$container_ip1)</p>\n\nenableSplunkWebSSL=0\n"
printf "$custom_web_conf1" > $PROJ_DIR/web1.conf
printf "loginBackgroundImageOption = custom\nloginCustomBackgroundImage = search:logincustombg/logo_SMT_green.png\n" >> $PROJ_DIR/web1.conf
CMD=`docker cp $PROJ_DIR/web1.conf $fullhostname1:/opt/splunk/etc/system/local/web.conf`

echo "Changing login screen server TEST02"
LINE2="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Hostname: </font><font color=\"#FF9033\"> $fullhostname2</font></H3>"
LINE3="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Host IP: </font><font color=\"#FF9033\"> $vip2</font></H3></CENTER>"
LINE5="<H2><CENTER><font color=\"#867979\">User: </font> <font color=\"red\">admin</font> &nbsp&nbsp<font color=\"#867979\">Password:</font> <font color=\"red\"> test0002</font></H2></font></CENTER><BR/>"
custom_web_conf2="[settings]\nlogin_content=<div align=\"right\" style=\"border:1px solid lime;\"> $LINE1 $LINE2 $LINE3 $LINE4 $LINE5 $LINE6 </div> <p>This data is auto-generated at container build time (container internal IP=$container_ip2)</p>\n\nenableSplunkWebSSL=0\n"
printf "$custom_web_conf2" > $PROJ_DIR/web2.conf
printf "loginBackgroundImageOption = custom\nloginCustomBackgroundImage = search:logincustombg/logo_SMT_green.png\n" >> $PROJ_DIR/web2.conf
CMD=`docker cp $PROJ_DIR/web2.conf $fullhostname2:/opt/splunk/etc/system/local/web.conf`

echo "Changing login screen server TEST03"
LINE2="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Hostname: </font><font color=\"#FF9033\"> $fullhostname3</font></H3>"
LINE3="<H3 style=\"text-align: left;\"><font color=\"#867979\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Host IP: </font><font color=\"#FF9033\"> $vip3</font></H3></CENTER>"
LINE5="<H2><CENTER><font color=\"#867979\">User: </font> <font color=\"red\">admin</font> &nbsp&nbsp<font color=\"#867979\">Password:</font> <font color=\"red\"> test0003</font></H2></font></CENTER><BR/>"
custom_web_conf3="[settings]\nlogin_content=<div align=\"right\" style=\"border:1px solid lime;\"> $LINE1 $LINE2 $LINE3 $LINE4 $LINE5 $LINE6 </div> <p>This data is auto-generated at container build time (container internal IP=$container_ip3)</p>\n\nenableSplunkWebSSL=0\n"
printf "$custom_web_conf3" > $PROJ_DIR/web3.conf
printf "loginBackgroundImageOption = custom\nloginCustomBackgroundImage = search:logincustombg/logo_SMT_green.png\n" >> $PROJ_DIR/web3.conf
CMD=`docker cp $PROJ_DIR/web3.conf $fullhostname3:/opt/splunk/etc/system/local/web.conf`

#restarting splunkweb 
echo "Restarting Splunk web interfaces"
CMD=`docker exec -u splunk -ti $fullhostname1 /opt/splunk/bin/splunk restart splunkweb -auth admin:test0001`
CMD=`docker exec -u splunk -ti $fullhostname2 /opt/splunk/bin/splunk restart splunkweb -auth admin:test0002`
CMD=`docker exec -u splunk -ti $fullhostname3 /opt/splunk/bin/splunk restart splunkweb -auth admin:test0003`

rm $PROJ_DIR/web?.conf
#-------web.conf stuff-------

