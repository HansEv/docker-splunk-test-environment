#!/bin/bash
for i in `seq 100  200`; do
			sudo ifconfig  lo0  10.0.0.$i 255.255.255.0 alias
done


#-----------splunk-net check---------------
SPLUNKNET="splunk-net"				#default name for network (host-to-host comm)
printf "Checking if docker network is created [$SPLUNKNET]...\n"
net=`docker network ls | grep $SPLUNKNET `
if [ -z "$net" ]; then
	printf "$\033[0;32m Creating...network\n"
	echo -e "\033[1m\033[0m"
    docker network create -o --iptables=true -o --ip-masq -o --ip-forward=true $SPLUNKNET
else
    printf " OK\n"
fi
#-----------end of splunk-net check---------------