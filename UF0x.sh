# Start up first Universal Forwarder (admin:changeme)
docker run -d \
--network=splunk-net --hostname=UF01 --name=UF01  \
-e SPLUNK_START_ARGS=--accept-license \
-e SPLUNK_SERVER_NAME=UF01 \
-e SPLUNK_USER=splunk  splunk/universalforwarder

# Start up second Universal Forwarder (admin:changeme)
docker run -d \
--network=splunk-net --hostname=UF02 --name=UF02  \
-e SPLUNK_START_ARGS=--accept-license \
-e SPLUNK_SERVER_NAME=UF02 \
-e SPLUNK_USER=splunk  splunk/universalforwarder
